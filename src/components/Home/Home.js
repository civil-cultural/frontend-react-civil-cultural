import React from 'react';
import Content from '../Content';
import image1 from './assets/images/image1.png';
import image2 from './assets/images/image2.png';
import image3 from './assets/images/image3.jpg';
import image4 from './assets/images/image4.png';
import image5 from './assets/images/image5.png';


function Home(props) {
    return (
        <React.Fragment>
          <div className="container">
            <div className="row mb-4">
              <div className="col box-create">
                <div className="tuk">
                  <span className="title">Criar publicação</span>
                </div>
                <textarea placeholder="Escreva algo......" defaultValue={""} className="conteudo" ></textarea>
                <div className="actions">
                  <button className="btn btn-primary btn-block">Publicar</button>
                </div>

              </div>
            </div>

            <hr/>
            <div className="row mb-4">
              <div className="col">
                
                <div className="user">
                  <img src={image3} width="50" className="user-photo" alt="image3" />
                  <div className="data-user">
                    <span className="username">Hernandes</span>
                    <span>em <a href="#!" className="link-post">Link do post</a></span>
                  </div>
                    <span className="comment-date">19/09/2019</span>
                </div>
                <p className="comment">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                  labore et dolore magna aliqua. Ut enim ad minim veniam, </p>
                  
                <div className="btn-group">
                  <button className="btn btn-primary"><a href="!#">curtir</a></button>
                  <button className="btn btn-secondary"><a href="!#">Comentar</a></button>
                </div>
                
              </div>
            </div>
            <hr/>
            <div className="row mb-4">
              <div className="col public box-create">
                <div className="tuk">
                  <span className="title">Faça um comentário:</span>
                </div>
                <textarea className="comment" placeholder="Escreva seu comentário aqui!"></textarea>
                <button className="btn btn-primary btn-block"><a>Comentar</a></button>
              </div>

            </div>
          </div>
        </React.Fragment>
    )
}

export default Home;




        