import { toast } from 'react-toastify'

export function success(msg, timeout, url) {
  toast.success(msg);
  setTimeout(() => {
    if (url) {
      if (process.env.NODE_ENV === 'development') {
          window.location.href = url;
      } else {
          window.location.href = '/';
      }
    }
  }, timeout);
}

export function fail(msg, timeout, url) {
  toast.error(msg);
  setTimeout(() => {
    if (url) {
      if (process.env.NODE_ENV === 'development') {
          window.location.href = url;
      } else {
          window.location.href = '/';
      }
    }
  }, timeout);
}
